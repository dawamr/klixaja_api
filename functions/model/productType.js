
module.exports = function(sequelize, DataTypes) {
  var productType = sequelize.define("productType", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     nameProductType: DataTypes.STRING,
     type:DataTypes.STRING,
     description:DataTypes.TEXT,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
      }
    }
  })

  return productType;
}