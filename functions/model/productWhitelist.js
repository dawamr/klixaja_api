
module.exports = function(sequelize, DataTypes) {
    var productWhitelist = sequelize.define("productWhitelist", {
       uuid:  { type: DataTypes.UUID, primaryKey: true },
       nameProductWhitelist: DataTypes.STRING,
       codeProductWhitelist:DataTypes.TEXT,
       typeProductWhitelist:DataTypes.TEXT,
       supplierProductWhitelist:DataTypes.TEXT,
       keterangan:DataTypes.TEXT,
       deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
        }
      }
    })
  
    return productWhitelist;
  }