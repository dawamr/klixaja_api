const functions = require("firebase-functions");
const users = require("./server/serverUsers")
const type = require('./server/serverType')
const store = require('./server/serverStore')
const cms = require('./server/serverCms')
const productCategory = require('./server/serverProductCategory')
const labelProduct = require('./server/serverLabelProduct')
const mainBanner = require('./server/serverMainBanner')
const fotoProduct = require('./server/serverFotoProduct')
const discount = require('./server/serverDiscount')
const productValidation = require('./server/serverProductValidation')
const admin = require('firebase-admin')


exports.users    = functions
                  .runWith({ memory: "2GB", timeoutSeconds: 120 })
                  .https
                  .onRequest(users);

exports.type    = functions
                  .runWith({ memory: "2GB", timeoutSeconds: 120 })
                  .https
                  .onRequest(type);

exports.store    = functions
                  .runWith({ memory: "2GB", timeoutSeconds: 120 })
                  .https
                  .onRequest(store);

exports.cms     = functions
                  .runWith({ memory: "2GB", timeoutSeconds: 120 })
                  .https
                  .onRequest(cms);

exports.productCategory     = functions
                              .runWith({ memory: "2GB", timeoutSeconds: 120 })
                              .https
                              .onRequest(productCategory);

exports.labelProduct     = functions
                              .runWith({ memory: "2GB", timeoutSeconds: 120 })
                              .https
                              .onRequest(labelProduct);

exports.mainBanner     = functions
                              .runWith({ memory: "2GB", timeoutSeconds: 120 })
                              .https
                              .onRequest(mainBanner);


exports.fotoProduct     = functions
                              .runWith({ memory: "2GB", timeoutSeconds: 120 })
                              .https
                              .onRequest(fotoProduct);

exports.discount     = functions
                              .runWith({ memory: "2GB", timeoutSeconds: 120 })
                              .https
                              .onRequest(discount);

exports.productValidation     = functions
                              .runWith({ memory: "2GB", timeoutSeconds: 120 })
                              .https
                              .onRequest(productValidation);