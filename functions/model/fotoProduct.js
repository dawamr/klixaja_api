
module.exports = function(sequelize, DataTypes) {
  var fotoProduct = sequelize.define("fotoProduct", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     productUri: DataTypes.TEXT,
     order:DataTypes.INTEGER,
     description:DataTypes.TEXT,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        fotoProduct.belongsTo(models.product)
      }
    }
  })

  return fotoProduct;
}