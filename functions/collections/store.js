const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const bcrypt = require('bcryptjs')
const mailgun = require('../helper/mailgun')
const moment = require('moment')

const listVendorAktif = () => {
    return model.vendors.findAll(
        {where:{deletedAt:null,statusVendor:'active'},
        include: [
            {
              model: model.users
            }
        ]
    }).then(result => {
        const listVendor = result.map(r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listVendor
    })
}

const ListStoreAktif = () => {
  return model.store.findAll(
    {where:{deletedAt:null,status:'active'},
    include: [
        {
          model: model.vendors
        }
    ]
  }).then(result => {
      const listStore = result.map(r => {
          const u = _.omit(r.get({plain: true}))
          u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
          u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
          return u
      })
      return listStore
  })
}

const ListStorePending = () => {
  return model.store.findAll(
    {where:{deletedAt:null,status:'pending'},
    include: [
        {
          model: model.vendors
        }
    ]
  }).then(result => {
      const listStore = result.map(r => {
          const u = _.omit(r.get({plain: true}))
          u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
          u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
          return u
      })
      return listStore
  })
}

const listVendorPending = () => {
    return model.vendors.findAll(
        {where:{deletedAt:null,statusVendor:'pending'},
        include: [
            {
              model: model.users
            }
        ]
    }).then(result => {
        const listVendor = result.map(r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listVendor
    })
}

const vendorOne = (uuid) => {
    return model.store.findOne({ where: {uuid} })
    .then(function (vendor) {
      vendor.get({plain: true}).createdAt = moment(vendor.get({plain: true}).createdAt).format('YYYY-MM-DD HH:mm:ss')
      vendor.get({plain: true}).updatedAt = moment(vendor.get({plain: true}).updatedAt).format('YYYY-MM-DD HH:mm:ss')
      return vendor.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const vendorUserUuidOne = (uuid) => {
  return model.vendors.findOne({ where: {userUuid:uuid} })
  .then(function (vendor) {
    vendor.get({plain: true}).createdAt = moment(vendor.get({plain: true}).createdAt).format('YYYY-MM-DD HH:mm:ss')
    vendor.get({plain: true}).updatedAt = moment(vendor.get({plain: true}).updatedAt).format('YYYY-MM-DD HH:mm:ss')
    return vendor.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}

const storeOne = (uuid) => {
  return model.vendors.findOne({ where: {uuid} })
    .then(function (vendor) {
      vendor.get({plain: true}).createdAt = moment(vendor.get({plain: true}).createdAt).format('YYYY-MM-DD HH:mm:ss')
      vendor.get({plain: true}).updatedAt = moment(vendor.get({plain: true}).updatedAt).format('YYYY-MM-DD HH:mm:ss')
      return vendor.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createVendor = async (data) => {
   
    return model.vendors.findOrCreate(
        {
            where: {userUuid:data.userUuid},
            defaults: {
                uuid: uuid.v4(),
                username:data.username,
                password:data.password,
                fullname:data.fullname,
                ktp:data.ktp,
                email:data.email,
                imageKTP:data.imageKTP,
                phone:data.phone,
                gender:data.gender,
                birthday:data.birthday,
                statusVendor:'pending',
                userUuid:data.userUuid
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const createStore = async (data) => {

  return model.store.findOrCreate(
    {
        where: {nameStore:data.nameStore},
        defaults: {
            uuid: uuid.v4(),
            nameStore:data.nameStore,
            address:data.address,
            phone:data.phone,
            description:data.description,
            saldo:0,
            longitude:data.longitude,
            latitude:data.latitude,
            category:data.category,
            status:'pending',
            vendorUuid:data.vendorUuid
        }
    })
    .spread(async(type, created) => {
        if (created) {
            return type.get({plain: true})
        }
        return created
    })
}

const aktifVendor = async(uuid) => {
    return model.vendors.findOne({ where: {uuid} })
    .then(async (vendor) => {

     const dataVendor = vendor.get({plain: true})

     const emailPayload = {
        subject:'Selamat Datang Mitra KlixAja',
        to:dataVendor.email,
        username:dataVendor.username,
        password:dataVendor.password,
        phone:dataVendor.phone
      }   

     await mailgun.sendActiveStore(emailPayload)

     const saltRounds = 10
     const password = await bcrypt.hash(dataVendor.password, saltRounds)
      
     
     await vendor.update({statusVendor:'active',password:password})

     const uuid = require('uuid')
     await model.userType.findOrCreate({
        where:{typeUuid:'07fbaa21-3a61-417c-9109-ac651a1b6075', userUuid:dataVendor.userUuid},
        defaults : {
            uuid:uuid.v4(),
            typeUuid:'07fbaa21-3a61-417c-9109-ac651a1b6075', //type store
            userUuid:dataVendor.userUuid
        }
       
     })
      
      return vendor.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const updateVendor = async(data) => {
    const dataVendorUpdate = {
      ...data.update
    }
    return model.vendors.findOne({ where: {uuid:data.uuid} })
    .then(async(vendor) => {
        vendor.update(dataVendorUpdate)
      return vendor.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const updateStore = async(data) => {
  const dataVendorUpdate = {
    ...data.updateStore
  }
  return model.store.findOne({ where: {uuid:data.uuid} })
  .then(async(vendor) => {
      vendor.update(dataVendorUpdate)
    return vendor.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}

const loginVendor = async(username, password) => {
    return model.vendors.findOne({ where: {username} })
    .then(async(vendor) => {
      const isVendor = await bcrypt.compare(password, vendor.get({plain: true}).password)
      if(isVendor){
        return vendor.get({plain: true})
      }else{
          return null
      }
      
    })
    .catch(e => {
      console.log(e)
    })
}

module.exports = {
    listVendorAktif,
    listVendorPending,
    vendorOne,
    loginVendor,
    aktifVendor,
    createVendor,
    updateVendor,
    ListStoreAktif,
    ListStorePending,
    storeOne,
    createStore,
    updateStore,
    vendorUserUuidOne
}