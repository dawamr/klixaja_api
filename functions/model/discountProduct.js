
module.exports = function(sequelize, DataTypes) {
  var discountProduct = sequelize.define("discountProduct", {
    uuid:  { type: DataTypes.UUID, primaryKey: true },
    status:DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        discountProduct.belongsTo(models.discount)
        discountProduct.belongsTo(models.product)
      }
    }
  })
 
  return discountProduct;
}