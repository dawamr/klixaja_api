
module.exports = function(sequelize, DataTypes) {
    var userType = sequelize.define("userType", {
      uuid:  { type: DataTypes.UUID, primaryKey: true },
      deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
            userType.belongsTo(models.users)
            userType.belongsTo(models.type)
        }
      }
    })
  
    return userType;
  }