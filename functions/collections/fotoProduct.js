const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listFotoProduct = () => {
    return model.fotoProduct.findAll(
      {
        where:{deletedAt:null}
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const fotoProductOne = (uuid) => {
    return model.fotoProduct.findOne(
      {
        where:{uuid}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createFotoProduct = async (data) => {
   
    return model.fotoProduct.findOrCreate(
        {
            where: {productUri:data.productUri},
            defaults: {
                uuid: uuid.v4(),
                productUri:data.productUri,
                description:data.description,
                productUuid:data.productUuid
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const updateFotoProduct = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.fotoProduct.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
   listFotoProduct,
   fotoProductOne,
   createFotoProduct,
   updateFotoProduct
}