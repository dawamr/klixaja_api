
module.exports = function(sequelize, DataTypes) {
    var groupStore = sequelize.define("groupStore", {
      uuid:  { type: DataTypes.UUID, primaryKey: true },
      name_group: DataTypes.STRING,
      longitude:DataTypes.TEXT,
      latitude:DataTypes.TEXT,
      description:DataTypes.TEXT,
      deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
            groupStore.belongsTo(models.store)
            groupStore.hasMany(models.orderProduct)
        }
      }
    })
   
    return groupStore;
  }