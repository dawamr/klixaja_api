
module.exports = function(sequelize, DataTypes) {
    var ingredientsProduct = sequelize.define("ingredientsProduct", {
      uuid:  { type: DataTypes.UUID, primaryKey: true },
      deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
            ingredientsProduct.belongsTo(models.ingredients)
            ingredientsProduct.belongsTo(models.product)
        }
      }
    })
  
    return ingredientsProduct;
  }