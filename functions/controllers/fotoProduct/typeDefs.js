const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type FotoProduct {
    uuid: String!
    productUri:String
    order:String
    description:String
    productUuid:String
    createAt:String
    updatedAt:String
    deletedAt:String
  }

  input createFotoProduct {
    productUri:String
    order:String
    description:String
    productUuid:String
  }

  input updateFotoProduct {
    uuid: String!
    update:fotoProductUpdate
  }

  input fotoProductUpdate {
    productUri:String
    order:String
    description:String
    productUuid:String
  }

  type Query {
    fotoProductList: [FotoProduct]
    fotoProductOne(uuid: String!): FotoProduct
  }

  type Mutation {
    createFotoProduct (createFotoProduct: createFotoProduct!):FotoProduct,
    updateFotoProduct (updateFotoProduct: updateFotoProduct!): FotoProduct
  }
`;

module.exports = { typeDefs };