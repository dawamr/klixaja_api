const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listDiscountProduct = () => {
    return model.discountProduct.findAll(
      {
        where:{deletedAt:null}
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const discountProductOne = (uuid) => {
    return model.discountProduct.findOne(
      {
        where:{uuid}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createDiscountProduct = async (data) => {
   
    return model.discountProduct.findOrCreate(
        {
            where: {productUuid:data.productUuid},
            defaults: {
                uuid: uuid.v4(),
                status:data.status || 'active',
                discountUuid: data.discountUuid,
                productUuid:data.productUuid
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const updateDiscountProduct = async(data) => {
    const dataTypeUpdate = {
      ...data.updateDiscountProduct
    }
    return model.discountProduct.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
  listDiscountProduct,
  discountProductOne,
  updateDiscountProduct,
  createDiscountProduct
}