
module.exports = function(sequelize, DataTypes) {
  var kurir = sequelize.define("kurir", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     namaKurir: DataTypes.STRING,
     type:DataTypes.STRING, 
     deletedAt: DataTypes.DATE
  }, {
    classMethods: { 
      associate: function(models) {
        kurir.hasMany(models.kurirProduct)
      }
    }
  })

  return kurir;
}