
module.exports = function(sequelize, DataTypes) {
    var withdraw = sequelize.define("withdraw", {
      uuid:  { type: DataTypes.UUID, primaryKey: true },
      statusApprovalWithdraw: DataTypes.STRING,
      nominalWithdraw:DataTypes.FLOAT,
      saldoStoreSebelumWithdraw:DataTypes.FLOAT,
      saldoStoreSetelahWithdraw:DataTypes.FLOAT,
      statusWithdraw:DataTypes.STRING,
      descriptionWithdraw: DataTypes.STRING,
      deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
            withdraw.belongsTo(models.storeBank)
            withdraw.hasMany(models.transaction)
        }
      }
    })
  
    return withdraw;
  }