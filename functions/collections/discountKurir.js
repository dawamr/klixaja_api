const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listDiscountKurir = () => {
    return model.discountKurir.findAll(
      {
        where:{deletedAt:null}
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const discountKurirOne = (uuid) => {
    return model.discountKurir.findOne(
      {
        where:{uuid}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createDiscountKurir = async (data) => {
   
    return model.discountKurir.findOrCreate(
        {
            where: {kurirProductUuid:data.kurirProductUuid},
            defaults: {
                uuid: uuid.v4(),
                status:data.status || 'active',
                discountUuid: data.discountUuid,
                kurirProductUuid:data.kurirProductUuid
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const updateDiscountKurir = async(data) => {
    const dataTypeUpdate = {
      ...data.updateDiscountKurir
    }
    return model.discountKurir.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
  listDiscountKurir,
  discountKurirOne,
  updateDiscountKurir,
  createDiscountKurir
}