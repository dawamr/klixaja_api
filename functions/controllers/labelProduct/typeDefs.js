const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type labelProduct {
    uuid: String!
    nameLabel:String
    description:String
    deletedAt:String
    createdAt:String
    updatedAt:String
  }

  input createLabelProduct {
    nameLabel:String
    description:String
  }

  input updateLabelProduct {
    uuid: String!
    update:labelProductUpdate
  }

  input labelProductUpdate {
    nameLabel:String
    description:String
  }

  type Query {
    labelProductList: [labelProduct]
    labelProductOne(uuid: String!): labelProduct
  }

  type Mutation {
    createLabelProduct (createLabelProduct: createLabelProduct!): labelProduct,
    updateLabelProduct (updateLabelProduct: updateLabelProduct!): labelProduct
  }
`;

module.exports = { typeDefs };