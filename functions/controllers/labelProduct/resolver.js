const modelLabelProduct = require('../../collections/labelProduct')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    labelProductList: () => modelLabelProduct.listLabelProduct().then(data => data),
    labelProductOne:(_, { uuid }) => modelLabelProduct.labelProductOne(uuid).then(data => data)
  },
  Mutation: {
    createLabelProduct:(_, args) => modelLabelProduct.createLabelProduct(args.createLabelProduct).then(data => data),
    updateLabelProduct:(_, args) => modelLabelProduct.updateLabelProduct(args.updateLabelProduct).then(data => data)
  }
};

module.exports = { resolvers }