
module.exports = function(sequelize, DataTypes) {
    var userCms = sequelize.define("userCms", {
      uuid:  { type: DataTypes.UUID, primaryKey: true },
      username: DataTypes.STRING,
      password: DataTypes.TEXT,
      phone: DataTypes.TEXT,
      status:DataTypes.STRING,
      roles:DataTypes.TEXT,
      deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
            
        }
      }
    })
  
    return userCms;
  }