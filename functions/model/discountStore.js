
module.exports = function(sequelize, DataTypes) {
  var discountStore = sequelize.define("discountStore", {
    uuid:  { type: DataTypes.UUID, primaryKey: true },
    status:DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        discountStore.belongsTo(models.discount)
        discountStore.belongsTo(models.store)
      }
    }
  })
 
  return discountStore;
}