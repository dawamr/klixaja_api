const modelMainBanner = require('../../collections/mainBanner')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    mainBannerList: () => modelMainBanner.listMainBanner().then(data => data),
    mainBannerOne:(_, { uuid }) => modelMainBanner.mainBannerOne(uuid).then(data => data),
    articleBannerOne:(_, { uuid }) =>  modelMainBanner.articleBannerOne(uuid).then(data => data)
  },
  Mutation: {
    createMainBanner:(_, args) => modelMainBanner.createMainBanner(args.createMainBanner).then(data => data),
    updateMainBanner:(_, args) => modelMainBanner.updateMainBanner(args.updateMainBanner).then(data => data),
    createArticleBanner:(_, args) => modelMainBanner.createArticleBanner(args.createArticleBanner).then(data => data),
    updateArticleBanner:(_, args) => modelMainBanner.updateArticleBanner(args.updateArticleBanner).then(data => data)
  }
};

module.exports = { resolvers }