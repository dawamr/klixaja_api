
module.exports = function(sequelize, DataTypes) {
    var store = sequelize.define("store", {
       uuid:  { type: DataTypes.UUID, primaryKey: true },
       nameStore: DataTypes.STRING,
       address:DataTypes.STRING,
       phone: DataTypes.TEXT,
       description:DataTypes.TEXT,
       saldo:DataTypes.TEXT,
       longitude:DataTypes.TEXT,
       latitude:DataTypes.TEXT,
       category:DataTypes.STRING,
       status:DataTypes.STRING,
       deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
            store.belongsTo(models.vendors)
            store.hasMany(models.groupStore)
            store.hasMany(models.discountStore)
            store.hasMany(models.storeBank)
        }
      }
    })
  
    return store;
  }