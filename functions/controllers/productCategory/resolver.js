const modelProductCategory = require('../../collections/productCategory')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    productCategoryList: () => modelProductCategory.listProductCategory().then(data => data),
    productCategoryOne:(_, { uuid }) => modelProductCategory.productCategoryOne(uuid).then(data => data)
  },
  Mutation: {
    createProductCategory:(_, args) => modelProductCategory.createProductCategory(args.createProductCategory).then(data => data),
    updateProductCategory:(_, args) => modelProductCategory.updateProductCategory(args.updateProductCategory).then(data => data)
  }
};

module.exports = { resolvers }