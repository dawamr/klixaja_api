const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listProductCategory = () => {
    return model.productCategory.findAll(
      {
        where:{deletedAt:null},
        include: [
         {
           model: model.productCategory,as:'parent',
           include:[{model:model.productCategory, as:'children'}]
           
         }
       ]
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const productCategoryOne = (uuid) => {
    return model.productCategory.findOne(
      {
        where:{uuid},
        include: [
         {
           model: model.productCategory,as:'parent'
           
         }
       ]
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createProductCategory = async (data) => {
   
    return model.productCategory.findOrCreate(
        {
            where: {nameProductCategory:data.nameProductCategory},
            defaults: {
                uuid: uuid.v4(),
                nameProductCategory: data.nameProductCategory,
                type:data.type,
                description:data.description, 
                parentId:data.parentId,
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const updateProductCategory = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.productCategory.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
    listProductCategory,
    productCategoryOne,
    createProductCategory,
    updateProductCategory
}