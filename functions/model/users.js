const _ = require('lodash')
const uuid = require('uuid')
const bcrypt = require('bcryptjs')

const saltRounds = 10

module.exports = function(sequelize, DataTypes) {
  var user = sequelize.define("users", {
    uuid:  { type: DataTypes.UUID, primaryKey: true },
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    name: DataTypes.STRING,
    telephone: DataTypes.STRING,
    address: DataTypes.TEXT,
    photo: DataTypes.TEXT,
    saldo: DataTypes.FLOAT,
    email: DataTypes.STRING,
    firebaseRegisterId:DataTypes.TEXT,
    birthday: DataTypes.TEXT,
    pin: DataTypes.TEXT,
    status:DataTypes.STRING,
    position: DataTypes.STRING,
    roles: DataTypes.TEXT,
    registerid: DataTypes.TEXT,
    deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
            user.hasMany(models.vendors)
            user.hasMany(models.userType)
            user.hasMany(models.orderProduct)
            user.hasMany(models.ratingProduct)
      },
      getAll: function () {
        return this.findAll()
        .then(function(users) {
          // return users after omitting it's credentials
          return users.map(u => _.omit(u.get({plain: true}), ['password']))
        })
        .catch(err => {
          console.log(err)
        })
      },
      findOneByUsername: function (username) {
        return this.findOne({ where: {username} })
        .then(function (user) {
          return user
        })
        .catch(err => {
          console.log(err)
        })
      },
      findOneByUuid: function (uuid) {
        return this.findOne({ where: {uuid} })
        .then(function (user) {
          return user
        })
        .catch(err => {
          console.log(err)
        })
      },
      createUser: function (username, password, name, telephone, email, roles, siteId,birthday, position ) {
        // preserve self to use inside promise
        var self = this

        return bcrypt.hash(password, saltRounds)
        .then(function (hash) {
          return self.findOrCreate(
          {
            where: {username},
            defaults: {
              uuid: uuid.v4(),
              username,
              name,
              telephone,
              email,
              roles,       
              siteId,
              birthday,
              position,        
              password: hash
            }
          })
          .spread(function (user, created) {
            if (created) return user.get({plain: true})

            return created
          })
        })
        .catch(err => {
          console.log(err)
        })
      },
      validateUser: function(username, password) {
        var self = this
        
        return this.findOne({ where: {username} })
        .then(function (user) {
          return bcrypt.compare(password, user.password)
        })
        .catch(err => {
          console.log(err)
        })
      },
      updateRegisterid: function (username,fcm){
        // preserve self to use inside promise
        var self = this

        return this.findOne({ where: {username} })
        .then(function (user) {
          user.update({registerid:fcm})
        return user
        })
        .catch(e => {
          console.log(e)
        })
      }
    },
    instanceMethods: {
      validatePassword: function (password) {
				return bcrypt.compare(password, this.password)
				.then(function (result) {
					return result
				})
        .catch(err => {
          console.log(err)
        })
      },
      updatePassword: function (password) {
        // preserve self to use inside promise
        var self = this

        return bcrypt.hash(password, saltRounds)
        .then(function (hash) {
          return self.update({password: hash})
        })
        .catch(function(e) {
          console.log(e)
        })
      },
      
    }
  })

  return user;
}