const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type UserCms {
    uuid: String!
    username:String
    password:String
    phone:String
    status:String
    roles:JSON
    deletedAt:String
    createdAt:String
    updatedAt:String
  }

  input createCMS {
    username:String
    password:String
    phone:String
    status:String
    roles:JSON
  }

  input updateCMS {
    uuid: String!
    update:cmsUpdate
  }

  input cmsUpdate {
    username:String
    password:String
    phone:String
    status:String
    roles:JSON
  }

  type Query {
    cmsList: [UserCms]
    cmsOne(uuid: String!): UserCms
    cmsLogin(username:String, password:String):UserCms
  }

  type Mutation {
    createCMS (createCMS: createCMS!): UserCms,
    updateCMS (updateCMS: updateCMS!): UserCms
  }
`;

module.exports = { typeDefs };