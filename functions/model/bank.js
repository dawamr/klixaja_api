
module.exports = function(sequelize, DataTypes) {
  var bank = sequelize.define("bank", {
    uuid:  { type: DataTypes.UUID, primaryKey: true },
    type: DataTypes.STRING,
    namaBank:DataTypes.TEXT,
    noRekening:DataTypes.TEXT,
    description:DataTypes.TEXT,
    status:DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
       bank.hasMany(models.storeBank)
      }
    }
  })
 
  return bank;
}