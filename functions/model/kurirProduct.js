
module.exports = function(sequelize, DataTypes) {
  var kurirProduct = sequelize.define("kurirProduct", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     price: DataTypes.FLOAT,
     type:DataTypes.STRING, 
     deletedAt: DataTypes.DATE
  }, {
    classMethods: { 
      associate: function(models) {
        kurirProduct.belongsTo(models.kurir)
        kurirProduct.belongsTo(models.product)
        kurirProduct.hasMany(models.discountKurir)
        kurirProduct.hasMany(models.orderProduct)
      }
    }
  })

  return kurirProduct;
}