const modelProductValidation = require('../../collections/productValidation')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    productWhitelistList: () => modelProductValidation.listProductWhitelist().then(data => data),
    productBlacklistList: () => modelProductValidation.listProductBlacklist().then(data => data),
    productWhitelistOne: (_, { uuid }) => modelProductValidation.productWhitelistOne(uuid).then(data => data),
    productWhitelistByCode: (_, { codeProduct }) => modelProductValidation.productWhitelistByCodeProduct(codeProduct).then(data => data),
    productBlacklistOne: (_, { uuid }) => modelProductValidation.productBlacklistOne(uuid).then(data => data),
    productBlacklistByCode: (_, { codeProduct }) => modelProductValidation.productBlacklistByCodeProduct(codeProduct),
    ingredientsList:()=> modelProductValidation.listIngredients().then(data => data),
    ingredientsOne:(_, { uuid }) => modelProductValidation.productIngredientsOne(uuid).then(data => data)
  },
  Mutation: {
    createProductWhitelist:(_, args) => modelProductValidation.createProductWhitelist(args.createProductWhitelist).then(data => data),
    createProductBlacklist:(_, args) => modelProductValidation.createProductBlackList(args.createProductBlacklist).then(data => data),
    updateProductWhitelist:(_, args) => modelProductValidation.updateProductWhitelist(args.updateProductWhitelist).then(data => data),
    updateProductBlacklist: (_, args) => modelProductValidation.updateProductBlacklist(args.updateProductBlacklist).then(data => data),
    createIngredients: (_, args) => modelProductValidation.createIngredients(args.createIngredients).then(data => data),
    updateIngredients: (_, args) => modelProductValidation.updateIngredients(args.updateIngredients).then(data => data)
  }
};

module.exports = { resolvers }