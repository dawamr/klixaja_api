let modelStore = require('../../collections/store')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    loginVendor: (_, { username, password }) => modelStore.loginVendor(username,password).then(data => data),
    VendorAktif: () => modelStore.listVendorAktif().then(data => data),
    VendorPending: () => modelStore.listVendorPending().then(data => data),
    StoreAktif: () => modelStore.ListStoreAktif().then(data => data),
    StorePending: () => modelStore.ListStorePending().then(data => data),
    vendorOne: (_, { uuid }) => modelStore.vendorOne(uuid).then(data => data),
    storeOne:(_, { uuid }) => modelStore.storeOne(uuid).then(data => data),
    activeVendor:(_, { uuid }) => modelStore.aktifVendor(uuid).then(data => data),
    vendorUserUuidOne:(_, { uuid }) => modelStore.vendorUserUuidOne(uuid).then(data => data)
  },
  Mutation: {
    createVendor:(_, args) => modelStore.createVendor(args.createVendor).then(data => data),
    createStore: (_, args) => modelStore.createStore(args.createStore).then(data => data),
    updateVendor:(_, args) => modelStore.updateVendor(args.updateVendor).then(data => data),
    updateStore: (_, args) => modelStore.updateStore(args.updateStore).then(data => data)
  }
};

module.exports = { resolvers }