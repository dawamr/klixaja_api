const modelUserCms = require('../../collections/userCms')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    cmsList: () => modelUserCms.listUserCms().then(data => data),
    cmsOne:(_, { uuid }) => modelUserCms.userCmsOne(uuid).then(data => data),
    cmsLogin:(_, { username, password }) => modelUserCms.loginUserCms(username,password).then(data => data)
  },
  Mutation: {
    createCMS:(_, args) => modelUserCms.createUserCms(args.createCMS).then(data => data),
    updateCMS:(_, args) => modelUserCms.updateUserCMS(args.updateCMS).then(data => data)
  }
};

module.exports = { resolvers }