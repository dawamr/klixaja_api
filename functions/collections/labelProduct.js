const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listLabelProduct = () => {
    return model.labelProduct.findAll(
      {
        where:{deletedAt:null}
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const labelProductOne = (uuid) => {
    return model.labelProduct.findOne(
      {
        where:{uuid}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createLabelProduct = async (data) => {
   
    return model.labelProduct.findOrCreate(
        {
            where: {nameLabel:data.nameLabel},
            defaults: {
                uuid: uuid.v4(),
                nameLabel:data.nameLabel,
                description:data.description
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const updateLabelProduct = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.labelProduct.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
   listLabelProduct,
   labelProductOne,
   createLabelProduct,
   updateLabelProduct
}