
module.exports = function(sequelize, DataTypes) {
    var productBlacklist = sequelize.define("productBlacklist", {
       uuid:  { type: DataTypes.UUID, primaryKey: true },
       nameProductBlacklist: DataTypes.STRING,
       codeProductBlacklist:DataTypes.TEXT,
       supplierProductBlacklist:DataTypes.TEXT,
       keterangan:DataTypes.TEXT,
       deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
        }
      }
    })
  
    return productBlacklist;
  }