let modelUser = require('../../collections/user')
let modelUserCms = require('../../collections/userCms')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    User: () => modelUser.listUser().then(data => data),
    UserOne: (_, { uuid }) => modelUser.userOne(uuid).then(data => data),
    Login: (_, { firebaseRegisterId})=> modelUser.loginUser(firebaseRegisterId).then(data => data),
    cmsLogin:(_, { username, password }) => modelUserCms.loginUserCms(username,password).then(data => data)
  },
  Mutation: {
    createUser:(_, args) => modelUser.createUser(args.createUser).then(data => data),
    googleSetup:(_, args) => modelUser.googleSetup(args.dataGmail).then(data => data),
    updateUser:(_, args) => modelUser.updateUser(args.updateUser).then(data => data)
  }
};

module.exports = { resolvers }