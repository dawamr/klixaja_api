const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listProductWhitelist = () => {
    return model.productWhitelist.findAll(
      {
        where:{deletedAt:null}
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}


const listProductBlacklist = () => {
    return model.productBlacklist.findAll(
      {
        where:{deletedAt:null}
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const listIngredients = () => {
  return model.ingredients.findAll(
    {
      where:{deletedAt:null}
    }
  ).then(result => {
      const listType = result.map(async r => {
          const u = _.omit(r.get({plain: true}))
          u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
          u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
          return u
      })
      return listType
  })
}

const productWhitelistByCodeProduct = (codeProduct) => {
    return model.productWhitelist.findOne(
      {
        where:{codeProductWhitelist:codeProduct}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const productBlacklistByCodeProduct = (codeProduct) => {
    return model.productBlacklist.findOne(
      {
        where:{codeProductBlacklist:codeProduct}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const productWhitelistOne = (uuid) => {
    return model.productWhitelist.findOne(
      {
        where:{uuid}
      }
    )
    .then(function (type) {
      console.log(type)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}


const productBlacklistOne = (uuid) => {
    return model.productBlacklist.findOne(
      {
        where:{uuid}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const productIngredientsOne = (uuid) => {
  return model.ingredients.findOne(
    {
      where:{uuid}
    }
  )
  .then(function (type) {
    return type.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}


const createProductWhitelist = async (data) => {
   
    return model.productWhitelist.findOrCreate(
        {
            where: {
                nameProductWhitelist:data.nameProductWhitelist,
                codeProductWhitelist:data.codeProductWhitelist
            
            },
            defaults: {
                uuid: uuid.v4(),
                nameProductWhitelist:data.nameProductWhitelist,
                codeProductWhitelist:data.codeProductWhitelist,
                typeProductWhitelist:data.typeProductWhitelist || '',
                supplierProductWhitelist:data.supplierProductWhitelist || '', 
                keterangan:data.keterangan || '',
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const createProductBlackList = async (data) => {
   
    return model.productBlacklist.findOrCreate(
        {
            where: {
                nameProductBlacklist:data.nameProductBlacklist,
                codeProductBlacklist:data.codeProductBlacklist
            
            },
            defaults: {
                uuid: uuid.v4(),
                nameProductBlacklist:data.nameProductBlacklist,
                codeProductBlacklist:data.codeProductBlacklist,
                supplierProductBlacklist:data.supplierProductBlacklist || '', 
                keterangan:data.keterangan || '',
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const createIngredients = async (data) => {
   
  return model.ingredients.findOrCreate(
      {
          where: {
              name:data.name
          },
          defaults: {
              uuid: uuid.v4(),
              name:data.name,
              value:data.value || '',
              type:data.type,
              description:data.description || ''
          }
      })
      .spread(async(type, created) => {
          if (created) {
              return type.get({plain: true})
          }
          return created
      })
}

const updateProductWhitelist = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.productWhitelist.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }


const updateProductBlacklist = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.productBlacklist.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

  const updateIngredients = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.ingredients.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
   listProductBlacklist,
   listProductWhitelist,
   productBlacklistByCodeProduct,
   productWhitelistByCodeProduct,
   productBlacklistOne,
   productWhitelistOne,
   createProductWhitelist,
   createProductBlackList,
   updateProductBlacklist,
   updateProductWhitelist,
   listIngredients,
   createIngredients,
   updateIngredients,
   productIngredientsOne
}