const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type Type {
    uuid: String!
    type: String
    description: String
    deletedAt: String
    createdAt: String
    updatedAt: String
  }

  input createType {
    type: String
    description: String
  }

  input updateType {
    uuid: String!
    update:userType
  }

  input userType {
    type: String
    description: String
    deletedAt: String
  }

  type Query {
    Type: [Type]
    typeOne(uuid: String!): Type
  }

  type Mutation {
    createType (createType: createType!): Type,
    updateType (updateType: updateType!): Type
  }
`;

module.exports = { typeDefs };