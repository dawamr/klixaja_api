const model = require('../model')
const _ = require('lodash')
const bcrypt = require('bcryptjs')
const uuid = require('uuid')
const mailgun = require('../helper/mailgun')
const auth = require('firebase-admin').auth()
const jwt = require('../helper/jwt')
const moment = require('moment')

const listUserCms = () => {
     return model.userCms.findAll(
       {
         where:{deletedAt:null}
       }
    ).then(result => {
        const users = result.map(r => {
            const u = r.get({plain: true})
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return users
    })
}

const userCmsOne = (uuid) => {
  return model.userCms.findOne(
    { where: {uuid:uuid}
     }
  )
  .then(function (user) {
    return user.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}

const loginUserCms = (username, password) => {

    return model.userCms.findOne({ where: {username} })
    .then(async(user) => {
      const pass = await bcrypt.compare(password, user.password)
      if(pass){
        const token = jwt.signToken(user.uuid)
        user.get({plain: true}).token = token
        return user.get({plain: true})
      }else{
        return false
      }
     
    })
    .catch(e => {
      return e
    })
}

const createUserCms = async (data) => {

    const saltRounds = 10
    const password = await bcrypt.hash(data.password, saltRounds)

    return model.userCms.findOrCreate(
      {
        where: {username:data.username},
        defaults: {
          uuid: uuid.v4(),
          password,
          phone:data.phone || '',
          status:data.status,
          roles:JSON.stringify(data.roles)
        }
      })
      .spread(async(user, created) => {
        if (created) {
          return user.get({plain: true})
        }else{
          return false
        }
        
      })
}

const updateUserCMS = async(data) => {
 
  if(data.update.roles) data.update.roles = JSON.stringify(data.update.roles)

  const saltRounds = 10

  if(data.update.password) data.update.password = bcrypt.hash(data.update.password, saltRounds)

  const dataUser = {
    ...data.update
  }
  
  return model.userCms.findOne({ where: {uuid:data.uuid} })
  .then(function (user) {
    user.update(dataUser)
    return user.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}



module.exports = {
    listUserCms,
    createUserCms,
    userCmsOne,
    loginUserCms,
    updateUserCMS
}