const model = require('../model')
const _ = require('lodash')
const bcrypt = require('bcryptjs')
const uuid = require('uuid')
const mailgun = require('../helper/mailgun')
const auth = require('firebase-admin').auth()
const jwt = require('../helper/jwt')
const moment = require('moment')

const listUser = () => {
     return model.users.findAll(
       {
         where:{deletedAt:null},
         include: [
          {
            model: model.userType,
            include: [{model:model.type}]
          }
        ]
       }
    ).then(result => {
        const users = result.map(r => {
            const u = r.get({plain: true})
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return users
    })
}

const userOne = (uuid) => {
  return model.users.findOne(
    { where: {uuid:uuid},
      include: [
        {
          model: model.userType,
          include: [{model:model.type}]
        }
      ]
     }
  )
  .then(function (user) {
    return user.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}

const loginUser = (firebaseRegisterId) => {

    return model.users.findOne({ where: {firebaseRegisterId} })
    .then(function (user) {
      const token = jwt.signToken(user.uuid)
      user.get({plain: true}).token = token
      return user.get({plain: true})
    })
    .catch(e => {
      return e
    })
}

const createUser = async (data) => {
   
    const dataCreate = {
      displayName:data.username,
      password:data.password,
      email:data.email
    }

    return auth.createUser(dataCreate).then(async (record) => {
      if(record.uid){
        const emailLink = await auth.generateEmailVerificationLink(record.email)
          const emailPayload = {
            subject:'Verivication Sign-up',
            link:emailLink,
            to:record.email
        }

        await mailgun.sendEmailVerivication(emailPayload)

        const saltRounds = 10
        const password = await bcrypt.hash(data.password, saltRounds)

        return model.users.findOrCreate(
          {
            where: {email:data.email},
            defaults: {
              uuid: uuid.v4(),
              firebaseRegisterId:record.uid,
              username:data.username,
              password,
              address:data.address,
              photo:'',
              name:data.name,
              telephone:data.telephone,
              email:data.email,
              birthday:data.birthday,
              pin:'1234',
              saldo:0.0,
              status:'process',
              position:data.position,
              roles:JSON.stringify(data.roles),
              registerid:''
            }
          })
          .spread(async(user, created) => {
            if (created) {
              await model.userType.create({
                uuid:uuid.v4(),
                typeUuid:'356a79b3-18a5-4c11-858a-855cf853b940', //type user
                userUuid:user.get({plain: true}).uuid
              })
              return user.get({plain: true})
            }
            return created
          })
      }else{
        return false
      }
    })
    .catch(err => {
      return err
    })
}

const updateUser = async(data) => {
 
  if(data.update.roles) data.update.roles = JSON.stringify(data.update.roles)

  const dataUser = {
    ...data.update
  }
  
  return model.users.findOne({ where: {uuid:data.uuid} })
  .then(function (user) {
    user.update(dataUser)
    return user.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}

const googleSetup = async(data) => {

  const saltRounds = 10
  const password = await bcrypt.hash(data.password || '12345', saltRounds)

  return model.users.findOrCreate(
    {
      where: {email:data.email},
      defaults: {
        uuid: uuid.v4(),
        firebaseRegisterId:data.uid,
        username:data.username,
        password,
        address:data.address,
        photo:data.photo,
        name:data.name,
        telephone:data.telephone,
        email:data.email,
        birthday:data.birthday,
        pin:'1234',
        saldo:0.0,
        status:'active',
        position:data.position,
        roles: data.roles ? JSON.stringify(data.roles): '',
        registerid:''
      }
    })
    .spread(async(user, created) => {
      const token = jwt.signToken(user.uuid)
      user.get({plain: true}).token = token
      if (created) {
        await model.userType.create({
          uuid:uuid.v4(),
          typeUuid:'356a79b3-18a5-4c11-858a-855cf853b940', //type user
          userUuid:user.get({plain: true}).uuid
        })
        return user.get({plain: true})
      }else{
        return user.get({plain: true})
      }
    })
}

module.exports = {
    listUser,
    createUser,
    userOne,
    loginUser,
    updateUser,
    googleSetup
}