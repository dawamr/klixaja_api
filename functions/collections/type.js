const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listType = () => {
    return model.type.findAll({where:{deletedAt:null}}).then(result => {
        const listType = result.map(r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const typeOne = (uuid) => {
    return model.type.findOne({ where: {uuid} })
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createType = async (data) => {
   
    return model.type.findOrCreate(
        {
            where: {type:data.type},
            defaults: {
                uuid: uuid.v4(),
                type:data.type,
                description:data.description
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const updateType = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.type.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
    listType,
    typeOne,
    createType,
    updateType
}