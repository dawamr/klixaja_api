
module.exports = function(sequelize, DataTypes) {
  var discount = sequelize.define("discount", {
    uuid:  { type: DataTypes.UUID, primaryKey: true },
    type: DataTypes.STRING,
    code:DataTypes.TEXT,
    nominal:DataTypes.STRING,
    description:DataTypes.TEXT,
    status:DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        discount.hasMany(models.discountStore)
        discount.hasMany(models.discountProduct)
        discount.hasMany(models.discountKurir)
        discount.hasMany(models.orderProduct)
      }
    }
  })
 
  return discount;
}