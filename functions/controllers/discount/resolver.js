const modelDiscount = require('../../collections/discount')
const modelDiscountKurir = require('../../collections/discountKurir')
const modelDiscountProduct = require('../../collections/discountProduct')
const modelDiscountStore = require('../../collections/discountStore')

const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    discountList: () => modelDiscount.listDiscount().then(data => data),
    discountOne:(_, { uuid }) => modelDiscount.discountOne(uuid).then(data => data),
    discountKurirList:() => modelDiscountKurir.listDiscountKurir().then(data => data),
    discountKurirOne:(_, { uuid }) => modelDiscountKurir.discountKurirOne(uuid).then(data => data),
    discountProductList:() => modelDiscountProduct.listDiscountProduct().then(data => data),
    discountProductOne:(_, { uuid }) => modelDiscountProduct.discountProductOne(uuid).then(data => data),
    discountStoreList:() => modelDiscountStore.listDiscountStore().then(data => data),
    discountStoreOne:(_, { uuid }) => modelDiscountStore.discountStoreOne(uuid).then(data => data)
  },
  Mutation: {
    createDiscount:(_, args) => modelDiscount.createDiscount(args.createDiscount).then(data => data),
    updateDiscount:(_, args) => modelDiscount.updateDiscount(args.updateDiscount).then(data => data),
    createDiscountKurir:(_, args) => modelDiscountKurir.createDiscountKurir(args.createDiscountKurir).then(data => data),
    updateDiscountKurir:(_, args) => modelDiscountKurir.updateDiscountKurir(args.updateDiscountKurir).then(data => data),
    createDiscountProduct:(_, args) => modelDiscountProduct.createDiscountProduct(args.createDiscountProduct).then(data => data),
    updateDiscountProduct:(_, args) => modelDiscountProduct.updateDiscountProduct(args.updateDiscountProduct).then(data => data),
    createDiscountStore:(_, args) => modelDiscountStore.createDiscountStore(args.createDiscountStore).then(data => data),
    updateDiscountStore:(_, args) => modelDiscountStore.updateDiscountStore(args.updateDiscountStore).then(data => data)
  }
};

module.exports = { resolvers }