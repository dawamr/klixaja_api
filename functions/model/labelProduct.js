
module.exports = function(sequelize, DataTypes) {
  var labelProduct = sequelize.define("labelProduct", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     nameLabel: DataTypes.STRING,
     description:DataTypes.TEXT,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
      }
    }
  })

  return labelProduct;
}