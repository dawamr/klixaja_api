
module.exports = function(sequelize, DataTypes) {
  var mainBanner = sequelize.define("mainBanner", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     bannerUri: DataTypes.TEXT,
     order:DataTypes.INTEGER,
     description:DataTypes.TEXT,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        mainBanner.belongsTo(models.articleBanner)
      }
    }
  })

  return mainBanner;
}