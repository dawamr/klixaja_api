
module.exports = function(sequelize, DataTypes) {
  var transaction = sequelize.define("transaction", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     tanggalPembayaran:DataTypes.TEXT,
     notes: DataTypes.TEXT,
     emailSend: DataTypes.INTEGER,
     saldoSebelumTransaksi:DataTypes.FLOAT,
     nominalPembayaran:DataTypes.FLOAT,
     saldoSetelahTransaksi:DataTypes.TEXT,
     status:DataTypes.STRING,
     konfirmasi:DataTypes.STRING,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
         transaction.belongsTo(models.orderProduct)
         transaction.belongsTo(models.withdraw)
      }
    }
  })

  return transaction;
}