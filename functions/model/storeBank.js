
module.exports = function(sequelize, DataTypes) {
  var storeBank = sequelize.define("storeBank", {
    uuid:  { type: DataTypes.UUID, primaryKey: true },
    status:DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        storeBank.belongsTo(models.bank)
        storeBank.belongsTo(models.store)
        storeBank.hasMany(models.withdraw)
      }
    }
  })
 
  return storeBank;
}