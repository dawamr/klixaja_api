const express = require('express')
var cors = require('cors')
const volleyball = require('volleyball')
const bodyparser = require('body-parser')
const { ApolloServer } = require('apollo-server-express')
const admin = require('firebase-admin')
const { apiKey } = require('../helper/config')
const secret = require('../helper/constant').secret
const jwt = require('jsonwebtoken')
const models = require('../model')

models.sequelize.sync()

// const serviceAccount = require('../klixaja-apps-firebase-adminsdk-t9v0a-e0f53fd99e.json')
 
// admin.initializeApp({
//     credential:admin.credential.cert(serviceAccount),
//     databaseURL: "https://klixaja-apps.firebaseio.com"
// })

admin.initializeApp()


var app = express()

app.use(volleyball)
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:false}))
app.use(cors())

const resolvers = require('../controllers/users/resolver').resolvers
const typeDefs = require('../controllers/users/typeDefs').typeDefs



const path = '/graphql';
new ApolloServer({
    resolvers,
    typeDefs,
    context: async ({req, res}) => {
        let msg = '';
        if(req.headers['x-api-key'] !== apiKey.toString()) {
           msg = 'wrong api key';
        }
        
        if(msg) return res.status(400).json({"error":msg})
        return true
    }
}).applyMiddleware({app,path})

module.exports = app

