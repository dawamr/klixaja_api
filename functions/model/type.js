
module.exports = function(sequelize, DataTypes) {
    var type = sequelize.define("type", {
      uuid:  { type: DataTypes.UUID, primaryKey: true },
      type: DataTypes.STRING,
      description: DataTypes.STRING,
      deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
            type.hasMany(models.userType)
        }
      }
    })
  
    return type;
  }