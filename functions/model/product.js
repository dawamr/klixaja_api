
module.exports = function(sequelize, DataTypes) {
  var product = sequelize.define("product", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     nameProduct: DataTypes.STRING,
     descriptionProduct:DataTypes.TEXT,
     priceProduct:DataTypes.FLOAT,
     weight:DataTypes.INTEGER,
     mainImage: DataTypes.TEXT,
     viewProduct:DataTypes.INTEGER,
     quantity: DataTypes.INTEGER,
     totalRating:DataTypes.STRING,
     stock: DataTypes.INTEGER,
     minQuantity: DataTypes.INTEGER,
     status: DataTypes.STRING,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        product.belongsTo(models.groupStore)
        product.belongsTo(models.store)
        product.belongsTo(models.productType)
        product.belongsTo(models.productCategory)
        product.belongsTo(models.labelProduct)
        product.hasMany(models.kurirProduct)
        product.hasMany(models.fotoProduct)
        product.hasMany(models.ingredientsProduct)
        product.hasMany(models.orderProduct)
        product.hasMany(models.discountProduct)
      }
    }
  })

  return product;
}