
module.exports = function(sequelize, DataTypes) {
  var discountKurir= sequelize.define("discountKurir", {
    uuid:  { type: DataTypes.UUID, primaryKey: true },
    status:DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        discountKurir.belongsTo(models.discount)
        discountKurir.belongsTo(models.kurirProduct)
      }
    }
  })
 
  return discountKurir;
}