const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type ingredients {
    uuid: String!
    name: String
    value: String
    type: String
    description: String
    createdAt: String
    updatedAt: String
    deletedAt: String
  }

  type productWhitelist {
    uuid: String!
    nameProductWhitelist: String
    codeProductWhitelist: String
    typeProductWhitelist: String
    supplierProductWhitelist: String
    keterangan: String
    createdAt:String
    updatedAt:String
  }

  type productBlacklist {
    uuid: String!
    nameProductBlacklist: String
    codeProductBlacklist: String
    supplierProductBlacklist: String
    keterangan: String
    createdAt:String
    updatedAt:String
  }

  input createProductWhitelist {
    nameProductWhitelist: String
    codeProductWhitelist: String
    typeProductWhitelist: String
    supplierProductWhitelist: String
    keterangan: String
  }

  input createProductBlacklist {
    nameProductBlacklist: String
    codeProductBlacklist: String
    supplierProductBlacklist: String
    keterangan: String
  }

  input createIngredients {
    name: String
    value: String
    type: String
    description: String
  }

  input updateProductWhitelist {
    uuid: String!
    update:productWhitelistUpdate
  }

  input updateProductBlacklist {
    uuid: String!
    update:productBlacklistUpdate
  }

  input updateIngredients {
    uuid: String!
    update:ingredientsUpdate
  }

  input ingredientsUpdate {
    name: String
    value: String
    type: String
    description: String
    createdAt: String
    updatedAt: String
    deletedAt: String
  }

  input productWhitelistUpdate {
    nameProductWhitelist: String
    codeProductWhitelist: String
    typeProductWhitelist: String
    supplierProductWhitelist: String
    keterangan: String
    deletedAt:String
  }

  input productBlacklistUpdate {
    nameProductBlacklist: String
    codeProductBlacklist: String
    supplierProductBlacklist: String
    keterangan: String
    deletedAt: String
  }

  type Query {
    productWhitelistList: [productWhitelist]
    productBlacklistList: [productBlacklist]
    productWhitelistOne(uuid: String!): productWhitelist
    productBlacklistOne(uuid: String!): productBlacklist
    productWhitelistByCode(codeProduct: String!): productWhitelist
    productBlacklistByCode(codeProduct: String!): productBlacklist
    ingredientsList: [ingredients]
    ingredientsOne(uuid: String!): ingredients
  }

  type Mutation {
    createProductWhitelist (createProductWhitelist: createProductWhitelist!): productWhitelist,
    createProductBlacklist (createProductBlacklist: createProductBlacklist!): productBlacklist,
    updateProductWhitelist (updateProductWhitelist: updateProductWhitelist!): productWhitelist,
    updateProductBlacklist (updateProductBlacklist: updateProductBlacklist!): productBlacklist,
    createIngredients (createIngredients: createIngredients!): ingredients
    updateIngredients (updateIngredients:updateIngredients): ingredients
  }
`;

module.exports = { typeDefs };