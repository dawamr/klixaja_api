# THIS IS KLIXAJA API REPOSITORY
==============================
```
- node version : v10.14.0
- npm version : 6.13.4
```

skeleton uploaded : 28 Juli 2020

example request :

```
https://us-central1-klixaja-apps.cloudfunctions.net/public/graphql
mutation {
   createstore(dataStore: {
       email:"store2@klix-apps.com"
       id_backoffice: "OFSLiM67Y4OHWaFgaYoVk04SmYT2"
       telephone:"081287620670"
       password: "candra123"
       id_category_store: "57aa9f33-e03f-4982-9e71-fac1c6cf6f20"
       address: "Minomartani Sleman"
       description:"toko penyedia komputer 2"
       name_store:"computer store 2"
   }){
       email
       id_backoffice
       telephone
       id_category_store
       address
       description
       name_store
   }
}
```