
module.exports = function(sequelize, DataTypes) {
  var orderProduct = sequelize.define("orderProduct", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     namaPemesan: DataTypes.STRING,
     nomerTelephonePemesan: DataTypes.TEXT,
     hargaProduct: DataTypes.FLOAT,
     typeProduct:DataTypes.STRING,
     ongkosKirim: DataTypes.FLOAT,
     discountProduct:DataTypes.FLOAT,
     totalPembayaran:DataTypes.FLOAT,
     typeDiscount:DataTypes.STRING,
     alamatPemesan: DataTypes.TEXT,
     kotaPemesan: DataTypes.TEXT,
     provinsiPemesan: DataTypes.TEXT,
     nomerResiPengiriman: DataTypes.TEXT,
     statusPemesan: DataTypes.TEXT,
     tanggalPengirimanProduk: DataTypes.TEXT,
     tanggalProsesProduk: DataTypes.TEXT,
     tanggalTerimaProduk:DataTypes.TEXT,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: { 
      associate: function(models) {
        orderProduct.hasMany(models.transaction)
        orderProduct.belongsTo(models.users)
        orderProduct.belongsTo(models.product)
        orderProduct.belongsTo(models.groupStore)
        orderProduct.belongsTo(models.discount)
        orderProduct.belongsTo(models.kurirProduct)
        orderProduct.belongsTo(models.ratingProduct)
      }
    }
  }) 

  return orderProduct;
}