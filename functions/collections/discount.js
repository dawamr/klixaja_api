const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listDiscount = () => {
    return model.discount.findAll(
      {
        where:{deletedAt:null}
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const discountOne = (uuid) => {
    return model.discount.findOne(
      {
        where:{uuid}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createDiscount = async (data) => {
   
    return model.discount.findOrCreate(
        {
            where: {code:data.code},
            defaults: {
                uuid: uuid.v4(),
                type:data.type,
                code:data.code,
                nominal:data.nominal,
                description:data.description,
                status:data.status || 'active'
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const updateDiscount = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.discount.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
  listDiscount,
  discountOne,
  createDiscount,
  updateDiscount
}