
module.exports = function(sequelize, DataTypes) {
  var vendor = sequelize.define("vendors", {
    uuid:  { type: DataTypes.UUID, primaryKey: true },
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    fullname:DataTypes.STRING,
    ktp:DataTypes.TEXT,
    imageKTP:DataTypes.TEXT,
    phone:DataTypes.TEXT,
    email:DataTypes.TEXT,
    level:DataTypes.TEXT,
    gender:DataTypes.STRING,
    birthday:DataTypes.TEXT,
    statusVendor:DataTypes.STRING,
    deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
            vendor.belongsTo(models.users)
            vendor.hasMany(models.store)
      }
    }
  })

  return vendor;
}