const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type Vendor {
      uuid: String!
      username:String
      password:String
      fullname:String
      ktp:String
      phone:String
      gender:String
      email:String
      level:String
      imageKTP:String
      birthday:String
      statusVendor:String
      deletedAt:String
      updatedAt:String
      createdAt:String
      userUuid:String
      user:Users
  }

  type Users {
    uuid: String!
    username:String
    password:String
    name:String
    telephone:String
    address:String
    photo:String
    token:String
    saldo:Float
    email:String
    status:String
    deletedAt: String
    createdAt: String
    updatedAt: String
    birthday:String
    pin:String
    position:String
    roles:JSON
    registerid:String
    userTypes:[userTypes]
  }

  type userTypes {
    uuid:String!
    typeUuid:String
    type: Types
  }

  type Types {
    uuid: String!
    type: String
    description: String
    deletedAt: String
    createdAt: String
    updatedAt: String
  }

  type Store {
    uuid:String
    nameStore:String
    address:String
    phone:String
    description:String
    saldo:Float
    longitude:String
    latitude:String
    vendor:Vendor
    category:String
    status:String
    createdAt:String
    updatedAt:String
    vendorUuid:String
  }

  input createStore {
    nameStore:String
    address:String
    phone:String
    description:String
    longitude:String
    latitude:String
    category:String
    createdAt:String
    updatedAt:String
    vendorUuid:String
  }

  input updateStore {
    uuid:String!
    updateStore:storeUpdate
  }
  
  input storeUpdate {
    nameStore:String
    address:String
    phone:String
    description:String
    saldo:Float
    longitude:String
    latitude:String
    category:String
    status:String
  }
  

  input createVendor {
      username:String
      password:String
      fullname:String
      imageKTP:String
      ktp:String
      email:String
      phone:String
      userUuid:String
      gender:String
      birthday:String
  }

  input updateVendor {
    uuid: String!
    update:vendorType
  }

  input vendorType {
      type: String
      username:String
      password:String
      fullname:String
      ktp:String
      imageKTP:String
      level:String
      email:String
      phone:String
      gender:String
      birthday:String
      statusVendor:String
      deletedAt: String
  }

  type Query {
    VendorAktif: [Vendor]
    VendorPending: [Vendor]
    vendorUserUuidOne(uuid: String!): Vendor
    StorePending: [Store]
    StoreAktif: [Store]
    vendorOne(uuid: String!): Vendor
    storeOne(uuid: String!): Store
    loginVendor(username:String, password:String):Vendor
    activeVendor(uuid: String!): Vendor
  }

  type Mutation {
    createVendor (createVendor: createVendor!): Vendor,
    updateVendor (updateVendor: updateVendor!): Vendor,
    createStore (createStore: createStore!): Store
    updateStore (updateStore: updateStore!): Store
  }
`;

module.exports = { typeDefs };