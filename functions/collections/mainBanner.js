const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listMainBanner = () => {
    return model.mainBanner.findAll(
      {
        where:{deletedAt:null},
        include: [
          {
            model: model.articleBanner
          }
        ]
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const mainBannerOne = (uuid) => {
    return model.mainBanner.findOne(
      {
        where:{deletedAt:null},
        include: [
          {
            model: model.articleBanner
          }
        ]
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const articleBannerOne = (uuid) => {
  return model.articleBanner.findOne(
    {
      where:{uuid}
    }
  )
  .then(function (type) {
    return type.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}

const createMainBanner = async (data) => {
   
    return model.mainBanner.findOrCreate(
        {
            where: {bannerUri:data.bannerUri},
            defaults: {
                uuid: uuid.v4(),
                bannerUri:data.bannerUri,
                order: data.order || 1,
                description:data.description,
                articleBannerUuid:data.articleBannerUuid
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const createArticleBanner = async (data) => {
   
  return model.articleBanner.findOrCreate(
      {
          where: {title:data.title},
          defaults: {
              uuid: uuid.v4(),
              title:data.title,
              description:data.description,
              imageUri:data.imageUri
          }
      })
      .spread(async(type, created) => {
          if (created) {
              return type.get({plain: true})
          }
          return created
      })
}

const updateMainBanner = async(data) => {
    const dataTypeUpdate = {
      ...data.update
    }
    return model.mainBanner.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const updateArticleBanner = async(data) => {
  const dataTypeUpdate = {
    ...data.updateArticle
  }
  return model.articleBanner.findOne({ where: {uuid:data.uuid} })
  .then(function (type) {
    type.update(dataTypeUpdate)
    return type.get({plain: true})
  })
  .catch(e => {
    console.log(e)
  })
}

module.exports = {
   listMainBanner,
   mainBannerOne,
   createMainBanner,
   updateMainBanner,
   createArticleBanner,
   updateArticleBanner,
   articleBannerOne
}