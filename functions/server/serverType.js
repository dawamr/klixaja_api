const express = require('express')
var cors = require('cors')
const volleyball = require('volleyball')
const bodyparser = require('body-parser')
const { ApolloServer } = require('apollo-server-express')
const admin = require('firebase-admin')
const { apiKey } = require('../helper/config')
const secret = require('../helper/constant').secret
const jwt = require('jsonwebtoken')


var app = express()

app.use(volleyball)
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:false}))
app.use(cors())

const resolvers = require('../controllers/type/resolver').resolvers
const typeDefs = require('../controllers/type/typeDefs').typeDefs



const path = '/graphql';
new ApolloServer({
    resolvers,
    typeDefs,
    context: async ({req, res}) => {
        let msg = '';
        if(req.headers['x-api-key'] !== apiKey.toString()) {
           msg = 'wrong api key';
        }
        
        jwt.verify(req.headers['authorization'],secret.toString(),(err, verifiedJwt)=> {
            if(err){
                msg = 'unathorized';
            }
        })
        
        if(msg) return res.status(400).json({"error":msg})
        return true
    }
}).applyMiddleware({app,path})

module.exports = app

