
module.exports = function(sequelize, DataTypes) {
  var productCategory = sequelize.define("productCategory", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     nameProductCategory: DataTypes.STRING,
     type:DataTypes.STRING,
     description:DataTypes.TEXT,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        productCategory.belongsTo(models.productCategory, {as:'parent', foreignKey:'parentId'})
        productCategory.hasMany(models.productCategory, {as:'children', foreignKey:'parentId'})
      }
    }
  })

  return productCategory;
}