const ingredientsProduct = require("./ingredientsProduct");

module.exports = function(sequelize, DataTypes) {
    var ingredients = sequelize.define("ingredients", {
      uuid:  { type: DataTypes.UUID, primaryKey: true },
      name: DataTypes.STRING,
      value: DataTypes.STRING,
      description:DataTypes.TEXT,
      type:DataTypes.TEXT,
      deletedAt: DataTypes.DATE
    }, {
      classMethods: {
        associate: function(models) {
            ingredients.hasMany(models.ingredientsProduct)
        }
      }
    })
  
    return ingredients;
  }