const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type Discount {
    uuid: String!
    type: String
    code: String
    nominal: String
    description: String
    status: String
    createAt:String
    updatedAt:String
    deletedAt:String
  }

  type DiscountKurir {
    uuid:String
    status:String
    discountUuid:String
    kurirProductUuid:String
    createAt:String
    updatedAt:String
    deletedAt:String
  }

  type DiscountProduct {
    uuid:String
    status:String
    discountUuid:String
    productUuid:String
    createAt:String
    updatedAt:String
    deletedAt:String
  }

  type DiscountStore {
    uuid:String
    status:String
    discountUuid:String
    storeUuid:String
    createAt:String
    updatedAt:String
    deletedAt:String
  }

  input createDiscount {
    type: String
    code: String
    nominal: String
    description: String
    status: String
  }

  input createDiscountKurir {
    status:String
    discountUuid:String
    kurirProductUuid:String
  }

  input createDiscountProduct {
    status:String
    discountUuid:String
    productUuid:String
  }

  input createDiscountStore {
    status:String
    discountUuid:String
    storeUuid:String
  }

  input updateDiscount {
    uuid: String!
    update:discountUpdate
  }

  input updateDiscountKurir {
    uuid: String!
    updateDiscountKurir:discountKurirUpdate
  }

  input updateDiscountProduct {
    uuid: String!
    updateDiscountProduct:discountProductUpdate
  }

  input updateDiscountStore {
    uuid: String!
    updateDiscountStore:discountStoreUpdate
  }

  input discountUpdate {
    type: String
    code: String
    nominal: String
    description: String
    status: String
  }

  input discountKurirUpdate {
    status:String
    discountUuid:String
    kurirProductUuid:String
  }

  input discountProductUpdate {
    status:String
    discountUuid:String
    productUuid:String
  }

  input discountStoreUpdate {
    status:String
    discountUuid:String
    storeUuid:String
  }

  type Query {
    discountList: [Discount]
    discountOne(uuid: String!): Discount
    discountKurirList: [DiscountKurir]
    discountKurirOne(uuid: String!): DiscountKurir
    discountProductList: [DiscountProduct]
    discountProductOne(uuid: String!): DiscountProduct
    discountStoreList:[DiscountStore]
    discountStoreOne(uuid: String!): DiscountStore
  }

  type Mutation {
    createDiscount (createDiscount: createDiscount!):Discount,
    updateDiscount (updateDiscount: updateDiscount!): Discount,
    createDiscountKurir(createDiscountKurir: createDiscountKurir!):DiscountKurir,
    updateDiscountKurir (updateDiscountKurir: updateDiscountKurir!): DiscountKurir,
    createDiscountProduct(createDiscountProduct: createDiscountProduct!):DiscountProduct,
    updateDiscountProduct (updateDiscountProduct: updateDiscountProduct!): DiscountProduct,
    createDiscountStore(createDiscountStore: createDiscountStore!):DiscountStore,
    updateDiscountStore (updateDiscountStore: updateDiscountStore!): DiscountStore
  }
`;

module.exports = { typeDefs };