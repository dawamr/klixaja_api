const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type productCategory {
    uuid: String!
    nameProductCategory: String
    type:String
    description:String
    parentId:String
    createdAt:String
    updatedAt:String
    parent:productCategory
  }

  input createProductCategory {
    nameProductCategory: String
    type:String
    description:String
    parentId:String
  }

  input updateProductCategory {
    uuid: String!
    update:productCategoryUpdate
  }

  input productCategoryUpdate {
    nameProductCategory: String
    type:String
    description:String
    parentId:String
    deletedAt:String
  }

  type Query {
    productCategoryList: [productCategory]
    productCategoryOne(uuid: String!): productCategory
  }

  type Mutation {
    createProductCategory (createProductCategory: createProductCategory!):productCategory,
    updateProductCategory (updateProductCategory: updateProductCategory!): productCategory
  }
`;

module.exports = { typeDefs };