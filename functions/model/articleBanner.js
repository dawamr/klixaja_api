
module.exports = function(sequelize, DataTypes) {
  var articleBanner = sequelize.define("articleBanner", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     title:DataTypes.STRING,
     description:DataTypes.TEXT,
     imageUri:DataTypes.TEXT,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
      }
    }
  })

  return articleBanner;
}