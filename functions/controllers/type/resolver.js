let modelType = require('../../collections/type')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    Type: () => modelType.listType().then(data => data),
    typeOne: (_, { uuid }) => modelType.typeOne(uuid).then(data => data)
  },
  Mutation: {
    createType:(_, args) => modelType.createType(args.createType).then(data => data),
    updateType:(_, args) => modelType.updateType(args.updateType).then(data => data)
  }
};

module.exports = { resolvers }