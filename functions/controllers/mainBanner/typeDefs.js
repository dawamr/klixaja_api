const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type mainBanner {
    uuid: String!
    bannerUri: String
    order: Int
    description:String
    deletedAt:String
    createdAt:String
    updatedAt:String
    articleBannerUuid:String
    articleBanner:articleBanner
  }

  type articleBanner {
    uuid:String
    title:String
    description:String
    imageUri:String
    deletedAt:String
    createdAt:String
    updatedAt:String
  }

  input createMainBanner {
    bannerUri: String
    articleBannerUuid:String
    order: Int
    description:String
  }

  input updateMainBanner {
    uuid: String!
    update:mainBannerUpdate
  }

  input mainBannerUpdate {
    bannerUri: String
    articleBannerUuid:String
    order: Int
    description:String
  }

  input createArticleBanner {
    title:String
    description:String
    imageUri:String
  }

  input updateArticleBanner {
    uuid:String!
    updateArticle:articleBannerUpdate
  }

  input articleBannerUpdate {
    title:String
    description:String
    imageUri:String
  }

  type Query {
    mainBannerList: [mainBanner]
    mainBannerOne(uuid: String!): mainBanner
    articleBannerOne(uuid: String!):articleBanner
  }

  type Mutation {
    createMainBanner (createMainBanner: createMainBanner!): mainBanner,
    createArticleBanner(createArticleBanner: createArticleBanner!): articleBanner,
    updateArticleBanner(updateArticleBanner:updateArticleBanner!): articleBanner,
    updateMainBanner (updateMainBanner: updateMainBanner!): mainBanner
  }
`;

module.exports = { typeDefs };