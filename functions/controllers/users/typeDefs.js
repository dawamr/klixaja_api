const { gql } = require('apollo-server-express')

const typeDefs = gql`
  scalar JSON

  type Users {
    uuid: String!
    username:String
    password:String
    name:String
    telephone:String
    address:String
    photo:String
    token:String
    saldo:Float
    status:String
    firebaseRegisterId:String
    deletedAt: String
    createdAt: String
    updatedAt: String
    email:String
    birthday:String
    pin:String
    position:String
    roles:JSON
    registerid:String
    userTypes:[userTypes]
  }

  type UserCms {
    uuid: String!
    username:String
    password:String
    phone:String
    token:String
    status:String
    roles:JSON
    deletedAt:String
    createdAt:String
    updatedAt:String
  }

  type userTypes {
    uuid:String!
    typeUuid:String
    type: Types
  }

  type Types {
    uuid: String!
    type: String
    description: String
    deletedAt: String
    createdAt: String
    updatedAt: String
  }

  input createUser {
    username:String
    password:String
    name:String
    telephone:String
    address:String
    photo:String
    saldo:Float
    status:String
    email:String
    birthday:String
    pin:String
    position:String
    roles:JSON
  }

  input updateUser {
    uuid: String!
    update:userUpdate
  }

  input userUpdate {
    username:String
    password:String
    name:String
    telephone:String
    address:String
    deletedAt: String
    photo:String
    firebaseRegisterId:String
    saldo:Float
    status:String
    email:String
    birthday:String
    pin:String
    position:String
    roles:JSON
    registerid:String
  }

  input dataGmail {
    username:String
    uid:String!
    vendor:String
    photo:String
    email:String
  }

  type Query {
    User: [Users]
    UserOne(uuid: String!): Users
    Login(firebaseRegisterId:String!):Users
    cmsLogin(username:String, password:String):UserCms
  }

  type Mutation {
    createUser (createUser: createUser!): Users,
    updateUser (updateUser: updateUser!): Users,
    googleSetup(dataGmail: dataGmail!): Users
  }
`;

module.exports = { typeDefs };