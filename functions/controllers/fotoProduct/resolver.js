const modelFotoProduct = require('../../collections/fotoProduct')
const GraphQLJSON = require('graphql-type-json')
const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    fotoProductList: () => modelFotoProduct.listFotoProduct().then(data => data),
    fotoProductOne:(_, { uuid }) => modelFotoProduct.fotoProductOne(uuid).then(data => data)
  },
  Mutation: {
    createFotoProduct:(_, args) => modelFotoProduct.createFotoProduct(args.createFotoProduct).then(data => data),
    updateFotoProduct:(_, args) => modelFotoProduct.updateFotoProduct(args.updateFotoProduct).then(data => data)
  }
};

module.exports = { resolvers }