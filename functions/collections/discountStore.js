const model = require('../model')
const _ = require('lodash')
const uuid = require('uuid')
const moment = require('moment')

const listDiscountStore = () => {
    return model.discountStore.findAll(
      {
        where:{deletedAt:null}
      }
    ).then(result => {
        const listType = result.map(async r => {
            const u = _.omit(r.get({plain: true}))
            u.createdAt = moment(u.createdAt).format('YYYY-MM-DD HH:mm:ss')
            u.updatedAt = moment(u.updatedAt).format('YYYY-MM-DD HH:mm:ss')
            return u
        })
        return listType
    })
}

const discountStoreOne = (uuid) => {
    return model.discountStore.findOne(
      {
        where:{uuid}
      }
    )
    .then(function (type) {
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
}

const createDiscountStore = async (data) => {
   
    return model.discountStore.findOrCreate(
        {
            where: {storeUuid:data.storeUuid},
            defaults: {
                uuid: uuid.v4(),
                status:data.status || 'active',
                discountUuid: data.discountUuid,
                storeUuid:data.storeUuid
            }
        })
        .spread(async(type, created) => {
            if (created) {
                return type.get({plain: true})
            }
            return created
        })
}

const updateDiscountStore = async(data) => {
    const dataTypeUpdate = {
      ...data.updateDiscountStore
    }
    return model.discountStore.findOne({ where: {uuid:data.uuid} })
    .then(function (type) {
      type.update(dataTypeUpdate)
      return type.get({plain: true})
    })
    .catch(e => {
      console.log(e)
    })
  }

module.exports = {
  listDiscountStore,
  discountStoreOne,
  updateDiscountStore,
  createDiscountStore
}