
module.exports = function(sequelize, DataTypes) {
  var ratingProduct = sequelize.define("ratingProduct", {
     uuid:  { type: DataTypes.UUID, primaryKey: true },
     rating:DataTypes.STRING,
     comment:DataTypes.TEXT,
     deletedAt: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        ratingProduct.belongsTo(models.product)
        ratingProduct.belongsTo(models.users)
        ratingProduct.hasMany(models.orderProduct)
      }
    }
  })

  return ratingProduct;
}